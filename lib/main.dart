import 'package:flutter/material.dart';
import 'package:minly_app/Screens/entro.dart';
import 'package:minly_app/Screens/login.dart';
import 'package:minly_app/Screens/sign_up.dart';
import 'Screens/home.dart';
import 'Screens/splach.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Splach(),
    );
  }
}
