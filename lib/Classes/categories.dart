import 'package:flutter/material.dart';

class categories{
  final IconData iconData;
  final String name;
  final Color color;

  categories(
  {
    this.color,
    this.iconData,
    this.name
}
      );
}