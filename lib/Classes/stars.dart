class Stars{
  final String name;
  final String desc;
  final String price;
  final String image;

  Stars({
    this.desc,
    this.name,
    this.price,
    this.image
});
}