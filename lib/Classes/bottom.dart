import 'package:flutter/cupertino.dart';

class Bottom {
  final String name;
  final Color color;
  final IconData iconData;

  Bottom(
  {
    this.iconData,
    this.color,
    this.name
}
      );
}