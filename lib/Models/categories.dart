import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Classes/categories.dart';

List<categories> category = [
  categories(
    name: 'Actors',
    color: Colors.deepOrange.shade600,
    iconData: Icons.mood
  ),
  categories(
    name: 'Musicians',
    color: Colors.purple.shade700,
    iconData: FontAwesomeIcons.music
  ),
  categories(
    name: 'Footballers',
    color: Colors.greenAccent.shade700,
    iconData:  FontAwesomeIcons.baseballBall
  ),
  categories(
    name: 'Comedians',
    color: Colors.yellow.shade700,
    iconData: Icons.sentiment_very_satisfied
  ),
  categories(
    name: 'Creators',
    color: Colors.teal.shade300,
    iconData: Icons.create
  ),
  categories(
    name: 'Actors',
    color: Colors.deepOrange,
    iconData: Icons.mood
  )
];