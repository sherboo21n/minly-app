import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Classes/bottom.dart';

List<Bottom> signupbottom = [
  Bottom(
    name:  'Sign up with Facebook',
    iconData: FontAwesomeIcons.facebookSquare,
    color: Colors.blue.shade900,
  ),
  Bottom(
    name:  'Sign up with Google',
    iconData: FontAwesomeIcons.google,
    color: Colors.lightBlue.shade700,
  ),
  Bottom(
    name:  'Sign up with Apple',
    iconData: FontAwesomeIcons.apple,
    color: Colors.black
  )
];