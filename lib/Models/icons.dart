import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Classes/icons.dart';

List<icons> icons2 = [
  icons(
    iconData: Icons.search
  ) ,
  icons(
    iconData: Icons.mood
  ) ,
  icons(
    iconData: FontAwesomeIcons.film
  ) ,
  icons(
    iconData: Icons.person_outline
  )
];