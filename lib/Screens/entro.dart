import 'package:flutter/material.dart';
import 'package:minly_app/Widgets/entro/entro_content.dart';
import 'package:minly_app/Widgets/entro/entro_image.dart';


class Entro extends StatefulWidget {
  @override
  _EntroState createState() => _EntroState();
}

class _EntroState extends State<Entro> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          EntroImage(),
        EntroContent()
        ],
      ),
    );
  }
}
