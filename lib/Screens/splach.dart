import 'dart:async';

import 'package:flutter/material.dart';
import 'package:minly_app/Screens/sign_up.dart';

import 'entro.dart';

class Splach extends StatefulWidget {
  @override
  _SplachState createState() => _SplachState();
}

class _SplachState extends State<Splach> {

  @override
  void initState() {
    Timer(
        Duration(seconds: 5) , (){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUp()));
    }
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:  Image.asset(
          'images/splach.jpg',
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
        ),
      )
    );
  }
}
