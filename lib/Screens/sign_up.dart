import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Widgets/form.dart';
import 'package:minly_app/Widgets/signup/sign_up_bottom.dart';
import 'package:minly_app/Widgets/signup/sign_up_content.dart';
import 'package:minly_app/Widgets/signup/sign_up_form.dart';

import 'login.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white60,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.grey.shade500,
              size: 30,
            ),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                  color: Colors.white60,
                  height: MediaQuery.of(context).size.height,
                  width:  MediaQuery.of(context).size.width
              ),
              Positioned(
                top: 0,
                left: 50,
                right: 35,
                child:Image.asset(
                  'images/minlylogo2.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                bottom: 0,
                child:Image.asset(
                    'images/minlyscreen2.jpg',
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height/4,
                    width:  MediaQuery.of(context).size.width
                ),
              ),
              SignUpContent(
                title: 'Welcome to Minly',
                desc:  '      Create happy Widgets.moments.         Spread joy. '
                    'Connect with your favorite celebrities.'
                    'Join us at Minly, and let the fun begin!',
              ),
              SignUpBottom(),
              SizedBox(
                height: 20,
              ),
             SignUpForm(),
              Positioned(
                bottom: 30,
                  right: 40,
                  left: 40,
                child:  bottom(
                  name: 'Sign Up',
                )
              ),
              Positioned(
                bottom: 0,
                  right: 40,
                  left: 40,
                child:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Already have an account?',
                      style: TextStyle(
                        color: Colors.pink.shade200
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) =>LogIn()));
                      },
                      child: Text(
                        'Log In',
                        style: TextStyle(
                            color: Colors.pink.shade200,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    )
                  ],
                )
              )
            ],
          ),
         Stack(
           children: <Widget>[
             Image.asset(
                 'images/minlyscreen3.jpg',
                 fit: BoxFit.cover,
                 height: MediaQuery.of(context).size.height/1/22,
                 width:  MediaQuery.of(context).size.width
             ),

           ],
         )
        ],
      )
    );
  }
}
