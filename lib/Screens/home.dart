import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:minly_app/Models/icons.dart';
import 'package:minly_app/Models/stars.dart';
import 'package:minly_app/Pages/moments.dart';
import 'package:minly_app/Pages/profile.dart';
import 'package:minly_app/Pages/search.dart';
import 'package:minly_app/Screens/login.dart';
import 'package:minly_app/Screens/sign_up.dart';
import 'package:transparent_image/transparent_image.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int _selectedIndex = 0;
  final _pages = [
   Search(),
    Moments(),
    LogIn(),
    Profile()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: Stack(
        children: <Widget>[
         _pages[_selectedIndex],
         Positioned(
           bottom: 0,
           child:  _BuildBottom(),
         )
        ],
      )
    );
  }
  Widget _BuildBottom(){
    return  Container(
      height: 50,
      margin: EdgeInsets.symmetric(
        horizontal: 70,
        vertical: 30
      ),
      padding: EdgeInsets.only(
        left: 35,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
          35
        )
      ),
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: icons2.length,
          itemBuilder: (context , index) =>Row(
            children: <Widget>[
              InkWell(
                onTap: (){
                  setState(() {
                    _selectedIndex = index ;
                  });
                },
                child: Icon(
                 icons2 [index].iconData,
                  color: _selectedIndex == index ? Colors.black :
                  Colors.grey.shade500,
                  size: _selectedIndex == index ? 30 : 27.5
                ),
              ),
              SizedBox(
                width: 30,
              )
            ],
          )
      )
    );
  }
}
