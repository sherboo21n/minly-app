import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Widgets/detaiels/deatiels_icons.dart';
import 'package:minly_app/Widgets/detaiels/detaiels_cont.dart';
import 'package:minly_app/Widgets/detaiels/detaiels_image.dart';

class StarDetaiels extends StatefulWidget {

  final String name , price , desc;
  StarDetaiels({this.name,this.desc,this.price});

  @override
  _StarDetaielsState createState() => _StarDetaielsState();
}

class _StarDetaielsState extends State<StarDetaiels> {

  bool _selected = false;
  final _icons = [
   Text(
     'f',
     style: TextStyle(
       fontSize: 32
     ),
   ),
    Icon(
        FontAwesomeIcons.twitter
    ),
    Icon(
        FontAwesomeIcons.instagram
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            child: ListView(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              children: <Widget>[
               Stack(
                 children: <Widget>[
                   DetaielsImage(),
                   Positioned(
                     top: 300,
                     left: 10,
                     child: IconButton(
                      icon: Icon(
                        _selected ? Icons.volume_off  : Icons.volume_up,
                        color: Colors.white,
                        size: 30,
                      ),
                       onPressed: (){
                        setState(() {
                          _selected = !_selected;
                        });
                       },
                     ),
                   ),
                   DetaielsCont(
                     name: widget.name,
                     desc: widget.desc,
                   ),
                 ],
               ),
                Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height / 2,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(
                                  35
                              ),
                              topLeft: Radius.circular(
                                  35
                              )
                          )
                      ),
                    ),
                    Positioned(
                      top: 0,
                      left: 22,
                      child: Row(
                          children: _icons.map((e) => Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.grey.shade700,
                                      width: 2.5
                                    )
                                ),
                                child: Center(
                                  child: e,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              )
                            ],
                          )
                          ).toList()
                      ),
                    ),
                    Positioned(
                      top: 60,
                      left: 20,
                      right: 20,
                      child: Container(
                        height: 120,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.amber.shade50,
                          borderRadius: BorderRadius.circular(
                            25
                          )
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                            left: 50,
                            right: 25
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                  right: 20
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.handshake,
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Text(
                                      'Share your kindness',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Your purchase has an impact. Surprise a'
                                    'friend or loved one with a personalized '
                                    '  video message from their favorite celebrity!',
                                style: TextStyle(
                                    color: Colors.grey.shade800,
                                    fontSize: 15
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 80,
                      left: 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Are you a business or brand?',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Drop us a line if you would like to',
                            style: TextStyle(
                                color: Colors.grey.shade900,
                                fontSize: 15
                            ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                'request a',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 15
                                ),
                              )
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'promotional video',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 15
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                'from Tamer Hosny!',
                                style: TextStyle(
                                    color: Colors.grey.shade900,
                                    fontSize: 15
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          DetaielsIcons(),
          Positioned(
            bottom: 30,
            left: 50,
            right: 50,
            child: _BuildBottom(),
          )
        ],
      )

    );
  }
  Widget _BuildBottom(){
    return  Container(
        height: 60,
       width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.purple.shade700,
            borderRadius: BorderRadius.circular(
                35
            )
        ),
        child: Center(
          child: Text(
            'Book now for EGP ' + widget.price,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          ),
        )
    );
  }
}
