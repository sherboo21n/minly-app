import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minly_app/Screens/sign_up.dart';
import 'package:minly_app/Widgets/form.dart';
import 'package:minly_app/Widgets/login/log_in_bottom.dart';
import 'package:minly_app/Widgets/login/log_in_form.dart';
import 'package:minly_app/Widgets/signup/sign_up_bottom.dart';
import 'package:minly_app/Widgets/signup/sign_up_content.dart';
import 'package:minly_app/Widgets/signup/sign_up_form.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white60,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.grey.shade500,
                size: 30,
              ),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                    color: Colors.white60,
                    height: MediaQuery.of(context).size.height,
                    width:  MediaQuery.of(context).size.width
                ),
                Positioned(
                  top: 0,
                  left: 50,
                  right: 35,
                  child:Image.asset(
                    'images/minlylogo2.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child:Image.asset(
                      'images/minlyscreen2.jpg',
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height/4,
                      width:  MediaQuery.of(context).size.width
                  ),
                ),
                SignUpContent(
                  title: 'Welcome back!',
                  desc: 'Log into your Minly account',
                ),
                LogInBottom(),
                LogInForm(),
                Positioned(
                  bottom: 150,
                  right: 40,
                  child: Text(
                    'Forget password?',
                    style: TextStyle(
                      color: Colors.grey.shade500
                    ),
                  ),
                ),
                Positioned(
                    bottom: 100,
                    right: 40,
                    left: 40,
                    child:  bottom(
                      name: 'Log In',
                    )
                ),
                Positioned(
                    bottom: 60,
                    right: 40,
                    left: 40,
                    child:  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Already have an account?',
                          style: TextStyle(
                              color: Colors.pink.shade200
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUp()));
                          },
                          child: Text(
                            'Sign Up',
                            style: TextStyle(
                                color: Colors.pink.shade200,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        )
                      ],
                    )
                )
              ],
            ),
          ],
        )
    );
  }
}
