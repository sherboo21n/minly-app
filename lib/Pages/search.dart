import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:minly_app/Models/categories.dart';
import 'package:minly_app/Models/stars.dart';
import 'package:minly_app/Screens/login.dart';
import 'package:minly_app/Widgets/search/search.dart';
import 'package:minly_app/Widgets/search/search_categories.dart';
import 'package:minly_app/Widgets/search/search_cont.dart';
import 'package:minly_app/Widgets/search/search_discover.dart';
import 'package:minly_app/Widgets/search/search_image.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body:  Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 340,
                      margin: EdgeInsets.only(
                          top: 100,
                          left: 40,
                          right: 40
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(
                              20
                          ),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(
                                  0, 0
                              ),
                              blurRadius: 5,
                              color: Colors.black12, // Black color with 12% opacity
                            )]
                      ),
                    ),
                    SearchCont(),
                    SearchImage(),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20
                  ),
                  child: Text(
                    'Categories',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
               SearchCategories(),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20
                  ),
                  child: Text(
                    'Discover',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                 Discover(),
              ],
            ),
          ),

          Positioned(
            top: 0,
            child: Container(
              height: 105,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey.shade200,
            ),
          ),
          search(),
        ],
      ),
    );
  }
}
