import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minly_app/Models/stars.dart';
import 'package:minly_app/Widgets/moments/moments_stack.dart';


class Moments extends StatefulWidget {
  @override
  _MomentsState createState() => _MomentsState();
}

class _MomentsState extends State<Moments> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: _BuildAppBar(),
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: stars.length,
          itemBuilder: (context , index) =>MomentsStack(
            name: stars[index].name,
            image: stars[index].image,
            index: index,
          )
      ),
    );
  }
  Widget _BuildAppBar(){
    return AppBar(
      backgroundColor: Colors.transparent,
      centerTitle: true,
      elevation: 0.0,
      title: Text(
        'Minly moments',
        style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 28
        ),
      ),
    );
  }
}
