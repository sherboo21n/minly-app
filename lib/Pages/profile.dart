import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:minly_app/Widgets/profile/profile_cont.dart';
import 'package:minly_app/Widgets/profile/profile_content.dart';


class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: Container(
          height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height/2,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height/2.3,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(
                                25
                            ),
                            bottomRight: Radius.circular(
                                25
                            )
                        )
                    ),
                  ),
                  ProfileCont(),
                  Positioned(
                    top: 140,
                    left: 60,
                    right: 50,
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.teal.shade500,
                        border: Border.all(
                          color: Colors.white,
                          width: 3
                        )
                      ),
                      child: Center(
                        child: Text(
                          'S',
                          style: TextStyle(
                            fontSize: 60,
                            color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  ),
                 ProfileContent(),
                ],
              ),
            ),
            Positioned(
              top: 320,
              left: 10,
              right: 10,
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height / 2,
                  padding: EdgeInsets.symmetric(
                    horizontal: 20
                  ),
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                     Row(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Container(
                           height: 40,
                           width: 40,
                           decoration: BoxDecoration(
                             color: Colors.greenAccent.shade100,
                             borderRadius: BorderRadius.circular(
                               10
                             )
                           ),
                           child: Center(
                             child: Icon(
                               FontAwesomeIcons.book,
                               color: Colors.green.shade700,
                             ),
                           ),
                         ),
                         SizedBox(
                           width: 25,
                         ),
                         Padding(
                           padding: const EdgeInsets.only(
                             top: 5
                           ),
                           child: Text(
                             'Minly wallet',
                             style: TextStyle(
                               color: Colors.black,
                               fontSize: 20,
                               fontWeight: FontWeight.bold
                             ),
                           ),
                         )
                       ],
                     ),
                      SizedBox(
                       height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 2
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  color: Colors.lightBlueAccent.shade100,
                                  borderRadius: BorderRadius.circular(
                                      10
                                  )
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.person,
                                  size: 25,
                                  color: Colors.lightBlueAccent.shade700,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 25,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 5
                              ),
                              child: Text(
                                'Edit profile',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 2
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  color: Colors.deepPurpleAccent.shade100,
                                  borderRadius: BorderRadius.circular(
                                      10
                                  )
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.settings,
                                  size: 25,
                                  color: Colors.deepPurple,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 25,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 5
                              ),
                              child: Text(
                                'Setting',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 2
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  color: Colors.deepOrangeAccent.shade100,
                                  borderRadius: BorderRadius.circular(
                                      10
                                  )
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.chat_bubble,
                                  size: 20,
                                  color: Colors.deepOrange,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 25,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 5
                              ),
                              child: Text(
                                'Edit profile',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
