import 'package:flutter/material.dart';
import 'package:minly_app/Models/log_in_form.dart';


class LogInForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Positioned(
        bottom: 170,
        left: 50,
        right: 40,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  '------------------------------------',
                  style: TextStyle(
                      color: Colors.grey.shade400
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'or',
                  style: TextStyle(
                      color: Colors.grey.shade400
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  '-----------------------------------',
                  style: TextStyle(
                      color: Colors.grey.shade400
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Column(
                children: form4.map((e) =>  Column(
                  children: <Widget>[
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: Colors.grey.shade400
                          )
                      ),
                      child: TextField(
                        decoration: InputDecoration(
                            enabledBorder: InputBorder.none,
                            hintText: e.hint,
                            helperStyle: TextStyle(
                                color: Colors.grey.shade400
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 15,
                                vertical: 15
                            ),
                            suffixIcon: Icon(
                                e.iconData
                            )
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                )
                ).toList()
            ),
          ],
        )
    );
  }
}