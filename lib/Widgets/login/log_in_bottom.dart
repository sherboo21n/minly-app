import 'package:flutter/material.dart';
import 'package:minly_app/Models/log_in_bottom.dart';

class LogInBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 160,
      right: 40,
      left: 40,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: loginbottom.map((e) => Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 50,
              decoration: BoxDecoration(
                  color: e.color,
                  borderRadius: BorderRadius.circular(
                      35
                  )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    e.iconData,
                    color: Colors.white,
                    size: 25,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    e.name,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            )
          ],
        )
        ).toList(),
      ),
    );
  }
}
