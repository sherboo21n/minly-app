import 'package:flutter/material.dart';

class SignUpContent extends StatelessWidget {

  final String title , desc;
  SignUpContent({this.title, this.desc});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 60,
        left: 70,
        right: 60,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 32,
                  fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
             desc,
              style: TextStyle(
                  color: Colors.grey.shade700,
                  fontSize: 16
              ),
            ),
          ],
        )
    );
  }
}
