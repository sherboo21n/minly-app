import 'package:flutter/material.dart';
import 'package:minly_app/Models/categories.dart';

class SearchCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: category.map((e) => Padding(
              padding: const EdgeInsets.only(
                  left: 20
              ),
              child:Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 55,
                        width: 55,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: e.color
                        ),
                        child: Center(
                          child: Icon(
                            e.iconData,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    e.name,
                    style: TextStyle(
                        color: Colors.black
                    ),
                  )
                ],
              )
          )
          ).toList()
      ),
    );
  }
}
