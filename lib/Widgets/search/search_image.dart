import 'package:flutter/material.dart';
import 'package:minly_app/Models/stars.dart';
import 'package:minly_app/Screens/login.dart';
import 'package:minly_app/Screens/star_detailes.dart';

class SearchImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 180,
        left: 20,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              children: stars.map((e) => Row(
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>StarDetaiels(
                        name: e.name,
                        desc: e.desc,
                        price: e.price,
                      )));
                    },
                    child: Container(
                      height: 200,
                      width: 170,
                      padding: EdgeInsets.only(
                          top: 150,
                        left: 10
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              20
                          ),
                        image: DecorationImage(
                          image: AssetImage(
                            e.image
                          ),
                          fit: BoxFit.cover
                        )
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            e.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            e.desc,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  )
                ],
              )

              ).toList()
          ),
        )
    );
  }
}
