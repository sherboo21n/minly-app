import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:minly_app/Models/stars.dart';

class Discover extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        crossAxisCount: 2,
        crossAxisSpacing: 0,
        mainAxisSpacing: 15,
        itemCount: stars.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(
                left: 10,
                right: 7
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                    20
                ),
                image: DecorationImage(
                  image: AssetImage(
                    stars[index].image,
                  ),
                  fit: BoxFit.cover,
                )
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(
                  Radius.circular(
                      20
                  )
              ),
              child: index == 3 || index == 5  ? Container(
                padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 15
                ),
                color: index == 3 ? Colors.deepOrange :Colors.purple.shade900,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        index == 3 ? Text(
                          'Create',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ) : Text(
                          'Make',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        index == 3 ? Text(
                          'happy',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ) : Text(
                          'someone',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        index == 3 ? Text(
                          'moments',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ) : Text(
                          'smile',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    ),
                    Icon(
                      Icons.format_quote,
                      color: Colors.white38,
                      size: 35,
                    )
                  ],
                ),
              ) : Padding(
                padding: EdgeInsets.only(
                    top: 180,
                    left: 10
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      stars[index].name,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      stars[index].desc,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        staggeredTileBuilder: (index) {
          return StaggeredTile.count(
              1, index == 3 || index == 5  ? 0.5: 1.1
          );
        });
  }
}
