import 'package:flutter/material.dart';

class search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Positioned(
        top: 45,
        right: 0,
        left: 0,
        child: Container(
          height: 50,
          margin: EdgeInsets.symmetric(
              horizontal: 20
          ),
          padding: EdgeInsets.only(
              left: 20,
              right: 20,
              top: 10,
              bottom: 5
          ),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(
                  35
              )
          ),
          child: TextField(
            decoration: InputDecoration(
              enabledBorder: InputBorder.none,
              prefixIcon: Row(
                children: <Widget>[
                  Image.asset(
                    'images/minly.jpg',
                    fit: BoxFit.cover,
                    width: 30,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    'Search for celebrities...',
                    style: TextStyle(
                        color: Colors.grey.shade300,
                        fontSize: 18
                    ),
                  )
                ],
              ),
              suffixIcon: Icon(
                Icons.search,
                color: Colors.grey.shade400,
              ),
            ),
          ),
        )
    );
  }
}
