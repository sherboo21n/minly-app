import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchCont extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 110,
      left: 60,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Featured',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 24
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Connect with your favorite celebrities',
            style: TextStyle(
                color: Colors.grey.shade900,
                fontSize: 14
            ),
          ),
         SizedBox(
           height: 240,
         ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'See all celebrities',
                style: TextStyle(
                    color: Colors.grey.shade900,
                    fontSize: 14
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.black,
                size: 20,
              )
            ],
          )
        ],
      ),
    );
  }
}
