import 'package:flutter/material.dart';
import 'package:minly_app/Screens/home.dart';

class bottom extends StatelessWidget {

  final String name;
  bottom({this.name});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) =>HomeScreen()));
      },
      child: Container(
        height: 45,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(
                35
            )
        ),
        child: Center(
          child: Text(
           name,
            style: TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        )
      ),
    );
  }
}
