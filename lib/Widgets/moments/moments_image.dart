import 'package:flutter/material.dart';

class MomentsImage extends StatelessWidget {

  final String  image;
  MomentsImage({this.image});

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height - 320,
      margin: EdgeInsets.only(
          left: 20,
        right: 20,
        top: 10
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(
                  25
              ),
              topLeft: Radius.circular(
                  25
              )
          ),
          image: DecorationImage(
              image: AssetImage(
                  image
              ),
              fit: BoxFit.cover
          )
      ),
    );
  }
}
