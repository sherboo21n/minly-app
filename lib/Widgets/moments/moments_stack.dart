import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'moments_content.dart';
import 'moments_image.dart';

class MomentsStack extends StatefulWidget {

  final String name , image;
  final int index;
  MomentsStack({this.name,this.image,this.index});

  @override
  _MomentsStackState createState() => _MomentsStackState();
}

class _MomentsStackState extends State<MomentsStack> {

   bool _selected = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height - 160,
          margin: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: 20,
              top: 20
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(
                25
            ),
          ),
        ),
        MomentsImage(
          image: widget.image,
        ),
        Positioned(
          top: 300,
          left: 30,
          child: IconButton(
            icon: Icon(
              _selected ? Icons.volume_off  : Icons.volume_up,
              color: Colors.white,
              size: 30,
            ),
            onPressed: (){
              setState(() {
                _selected = !_selected;
              });
            },
          ),
        ),
       MomentsContent(
         name: widget.name,
         index: widget.index,
         image: widget.image,
       ),
      ],
    );
  }
}



