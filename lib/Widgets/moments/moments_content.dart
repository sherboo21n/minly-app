import 'package:flutter/material.dart';

class MomentsContent extends StatelessWidget {

  final int index ;
  final String image , name;
  MomentsContent({this.index,this.name,this.image});

  @override
  Widget build(BuildContext context) {
    return  Positioned(
      bottom: 35,
      left: 40,
      right: 40,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                backgroundImage: AssetImage(
                  image,
                ),
                radius: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 10
                ),
                child: Text(
                  name,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16
                  ),
                ),
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.only(
                    bottom: 10
                ),
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                    color: index.isOdd? Colors.deepOrange : Colors.purple.shade900 ,
                    borderRadius: BorderRadius.circular(
                        30
                    )
                ),
                child: Center(
                  child: Text(
                    'Book',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            name + ' sends special birthday wishes to'
                ' Nany shady Minly video was gifted from'
                ' Nour El Kattan.',
            style: TextStyle(
              color: Colors.grey.shade900,
              fontSize: 16
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.save_alt,
                color: Colors.black,
                size: 25,
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 5
                ),
                child: Text(
                  'Share Minly',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                  ),
                ),
              )
            ],
          )
        ],
      )
    );
  }
}
