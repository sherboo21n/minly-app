import 'package:flutter/material.dart';
import 'package:minly_app/Screens/sign_up.dart';

class EntroContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return   Positioned(
      bottom: 50,
      left: 20,
      right: 55,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'm.',
            style: TextStyle(
                color: Colors.grey.shade300,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                fontSize: 30
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'create',
            style: TextStyle(
                color: Colors.black,
                fontSize: 30
            ),
          ),
          Text(
            'Widgets.moments.',
            style: TextStyle(
                color: Colors.grey.shade300,
                fontWeight: FontWeight.bold,
                fontSize: 30
            ),
          ),
          Text(
            'deliver',
            style: TextStyle(
                color: Colors.black,
                fontSize: 30
            ),
          ),
          Text(
            'happiness',
            style: TextStyle(
                color: Colors.grey.shade300,
                fontSize: 30
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Custom video shoutouts from your favorite celebrities',
            style: TextStyle(
                color: Colors.grey.shade300,
                fontSize: 24
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUp()));
                },
                child: Container(
                  height: 50,
                  width: 110,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(
                          35
                      )
                  ),
                  child: Center(
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                          color: Colors.black
                      ),
                    ),
                  ),
                ),
              ) ,
              SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUp()));
                },
                child: Container(
                  height: 50,
                  width: 110,
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(
                          35
                      ),
                      border: Border.all(
                          color: Colors.grey.shade300
                      )
                  ),
                  child: Center(
                    child: Text(
                      'Log In',
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
