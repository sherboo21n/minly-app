import 'package:flutter/material.dart';

class ProfileContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Positioned(
      bottom: 60,
      left: 40,
      right: 50,
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 20
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'EGP 0.00',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Available balance',
                    style: TextStyle(
                        color: Colors.grey.shade600
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 30
              ),
              child: VerticalDivider(
                color: Colors.grey,
                width: 90,
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.mood_bad,
                      color: Colors.yellow,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Minly videos',
                      style: TextStyle(
                          color: Colors.grey.shade600
                      ),
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}
