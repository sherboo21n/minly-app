import 'package:flutter/material.dart';

class ProfileCont extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/3.5,
      padding: EdgeInsets.only(
          top: 70
      ),
      decoration: BoxDecoration(
          color: Colors.pink,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(
                  25
              ),
              bottomRight: Radius.circular(
                  25
              )
          )
      ),
      child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Sher Boo',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 24
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                'sherboo21n@gmail.com',
                style: TextStyle(
                    color: Colors.grey.shade100,
                    fontSize: 14
                ),
              )
            ],
          )
      ),
    );
  }
}
