import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetaielsIcons extends StatelessWidget {


  final _icons = [
    Icon(
      Icons.arrow_back_ios
    ),
    Icon(
      Icons.save_alt
    ),

  ];

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 30,
      left: 10,
      right: 10,
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _icons.map((e) => Row(
          children: <Widget>[
            InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Container(
                height: 50,
                width: 35,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white
                ),
                child: Center(
                  child: Icon(
                    e.icon,
                    size: 20,
                    color: Colors.black,
                  ),
                ),
              ),
            )
          ],
        ),
        ).toList()
      )
    );
  }
}
