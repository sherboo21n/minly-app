import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetaielsCont extends StatefulWidget {

  final String name , desc;
  DetaielsCont({this.desc,this.name});

  @override
  _DetaielsContState createState() => _DetaielsContState();
}

class _DetaielsContState extends State<DetaielsCont> {


  final _content = [
    Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.star,
              color: Colors.yellow.shade900,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow.shade900,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow.shade900,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow.shade900,
            ),
            Icon(
              Icons.star,
              color: Colors.yellow.shade900,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              '5.0',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '1 review',
              style: TextStyle(
                  color: Colors.grey.shade700
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Container(
              height: 13,
              width: 15,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey.shade600,
              ),
              child: Center(
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 10,
                  color: Colors.white,
                ),
              ),
            )
          ],
        )
      ],
    ),
    Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.timer,
              color: Colors.grey.shade900,
              size: 15,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              '3 days',
              style: TextStyle(
                  color: Colors.black
              ),
            )
          ],
        ),
        Text(
          'response time',
          style: TextStyle(
              color: Colors.grey.shade700
          ),
        )
      ],
    )
  ];

  bool _show = false;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
          height: MediaQuery.of(context).size.height - 350,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(
                      35
                  ),
                  topLeft: Radius.circular(
                      35
                  )
              )
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 20,
                top: 10
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 5,
                  margin: EdgeInsets.symmetric(
                      horizontal: 155,
                      vertical: 5
                  ),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade400,
                      borderRadius: BorderRadius.circular(
                          30
                      )
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  widget.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 28
                  ),
                ),
                Text(
                  widget.desc,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 18
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                    children: _content.map((e) => Container(
                        height: 60,
                        width: 170,
                        margin: EdgeInsets.symmetric(
                          horizontal: 5,
                        ),
                        padding: EdgeInsets.all(
                            10
                        ),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            borderRadius: BorderRadius.circular(
                                20
                            )
                        ),
                        child: e
                    )
                    ).toList()
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Singer Actor, Composer, Songwriter, Author, and '
                      'Producer. First Arab Artist in history to get his '
                      'handprint & footprint in the Chinese theater ,',
                  style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 16
                  ),
                ),
               InkWell(
                 onTap: (){
                   setState(() {
                     _show = !_show;
                   });
                 },
                 child: _show ? Text(
                   'Hollywood',
                   style: TextStyle(
                       color: Colors.grey.shade900,
                       fontSize: 16
                   ),
                 )   : Text(
                   'Read more',
                   style: TextStyle(
                       color: Colors.purple.shade700,
                       fontSize: 16
                   ),
                 ),
               ),
                _show ? Text(
                  'Read Less',
                  style: TextStyle(
                      color: Colors.purple.shade700,
                      fontSize: 16
                  ),
                )  :Text(
                  ''
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Follow Tamer Hosny',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }
}
